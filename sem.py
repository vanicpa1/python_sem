from PIL import ImageTk
from PIL import Image as img
import numpy as np
import sys
from tkinter import *

arr1 = np.array

def DisplayPicture (obr, label):
    global arr1
    arr1 = np.copy(obr)
    im1 = img.fromarray(obr)
    t_img = ImageTk.PhotoImage(im1)
    label.configure(image=t_img)
    label.image=t_img
    label.pack(side='bottom')
    
def mirror (obr, label):
    obr1 = np.flip(obr, 1)
    
    DisplayPicture(obr1, label)

def rotatel (obr, label):
    obr = np.rot90(obr,1,axes=(0,1))
    DisplayPicture(obr,label)
    
def rotater (obr, label):
    obr = np.rot90(obr,-1,axes=(0,1))
    DisplayPicture(obr,label)

def negativ1 (obr):
     obr = 255 - obr
     return obr
        
def negativ (obr, label):
    obr = 255 - obr
    DisplayPicture(obr, label)
    
def Light (obr, label):
    obr = obr + negativ1(obr)//3
    DisplayPicture(obr, label)

def Dark (obr, label):
    obr = obr//2
    DisplayPicture(obr, label)

def ShadesOfGray (obr, label):
    
    arr = [0.2989, 0.5870, 0.1140]
    obr = obr * arr
    obr = np.sum(obr, axis=2)
    DisplayPicture(obr, label)

def HighlightEdges (obr, label):
    cop = np.copy(obr)
   
    
    for x in range(1, (obr.shape[0] - 1)):
        for y in range(1, obr.shape[1] - 1):
            for z in range(0, obr.shape[2]):
                cop = (
                    + 9 * obr[x, y, z]
                    - obr[x - 1, y, z]
                    - obr[x + 1, y, z]
                    - obr[x, y + 1, z]
                    - obr[x, y - 1, z]
                    - obr[x - 1, y + 1, z]
                    - obr[x - 1, y - 1, z]
                    - obr[x + 1, y - 1, z]
                    - obr[x + 1, y + 1, z])
    
                if cop < 0:
                    cop = 0
                if cop > 255:
                    cop = 255
                obr[x][y][z] =  cop
    
               
    DisplayPicture(obr, label)


def main():    
 
    root = Tk()
    root.title('Photo')

    im = img.open(sys.argv[1])
    arr = np.array(im)
    global arr1
    arr1 = np.copy(arr)

    t_img = ImageTk.PhotoImage(im)
    label = Label(root, image=t_img)
    label.pack(side='bottom')
    
    Button(root, text='ZRCADLO', width=10, command=lambda:mirror(arr1, label)).pack(side='right')
    Button(root, text='ORIGINAL', width=10, command=lambda:DisplayPicture(arr, label)).pack(side='right')
    Button(root, text='ROTACE R', width=10, command=lambda:rotater(arr1, label)).pack(side='right')
    Button(root, text='ROTACE L', width=10, command=lambda:rotatel(arr1, label)).pack(side='right')
    
    Button(root, text='SEDA', width=10, command=lambda:ShadesOfGray(arr1, label)).pack(side='left')
    Button(root, text='SVETLA', width=10, command=lambda:Light(arr1, label)).pack(side='left')
    Button(root, text='INVERZE', width=10, command=lambda:negativ(arr1, label)).pack(side='left')
    Button(root, text='TMAVA', width=10, command=lambda:Dark(arr1, label)).pack(side='left')
    Button(root, text='HRANY', width=10, command=lambda:HighlightEdges(arr1, label)).pack(side='left')
    root.mainloop()
    
if __name__ == '__main__':
    main()
